import React, { Component } from 'react'
import { View, Text, Button } from '@tarojs/components'

import './index.less'

export default function Login () {

  return (
    <View className='login'>
      <Button className="lo-btn login-btn clear-button" hoverClass="lo-hover-btn">微信一键登录</Button>
    </View>
  )
}
