import React, { Component } from 'react'
import Taro from '@tarojs/taro'
import { View, Image,Text } from '@tarojs/components'
import IconFont from '../../../iconfont'

import './index.less'
const boy = require('../../assets/images/boy.png')

export default function Mine () {

  const goLogin = () => {
    Taro.navigateTo({url: '/pages/login/index'});
  }

  return (
    <View className='mine'>
      <View className="user flex-cll-center">
        <Image className="u-avatar" src={boy}></Image>
        <View className="u-login login-btn" hoverClass="u-hover-login" onClick={() => goLogin()}>登录</View>
        {/* <Text className="u-name">what's your name</Text> */}
      </View>

      <View className="list">
        <View className="l-item flex-rwa-center">
          <Text className="label">组织活动</Text>
          <IconFont name="announcementspeaker" size={40}></IconFont>
        </View>

        <View className="l-item flex-rwa-center">
          <Text className="label">活动记录</Text>
          <IconFont name="chevron-right" size={40}></IconFont>
        </View>

      </View>
    </View>
  )
}
