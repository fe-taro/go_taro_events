import React, { useState } from 'react'
import Taro, { useDidShow } from '@tarojs/taro'
import { View, Text, Swiper, SwiperItem, Image } from '@tarojs/components'
import * as demo from '../../utils/ros'
import './index.less'

export default function Index () {
  let [swList, setSwList] = useState([])

  const goDetails = (id) => {
    Taro.navigateTo({
      url: `/subpages/expages/details/index?id=${id}`
    })
  }

  useDidShow(() => {
    console.log('page init', demo);
    let tempDemo = [];
    for (let key of Object.keys(demo)) {
      tempDemo = [...tempDemo, ...[{id:0, logo: demo[key], desc: 'one of the event'}]];
    }
    console.log('page init', tempDemo);
    setSwList(tempDemo)
  })


  return (
    <View className='index'>
      <View className="ac-list">
        {
          swList.map((item) => (
            <View className="ac-card flex-cll-center" onClick={() => goDetails(item.id)}>
              <Image mode="aspectFill" src={item.logo}></Image>
              <Text>{item.desc}</Text>
            </View>
          ))
        }
      </View>
  </View>
  )
}

