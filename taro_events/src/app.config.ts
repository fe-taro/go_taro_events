// src/app.config.js
import { useGlobalIconFont } from '../iconfont/helper';

export default {
  pages: [
    'pages/index/index',
    'pages/mine/index',
    'pages/login/index',
  ],
  subpackages: [
    {
      root: 'subpages/expages',
      pages: [
        'details/index',
        'record/index'
      ]
    },
  ],
  window: {
    navigationBarTextStyle: 'black',
    navigationBarBackgroundColor: '#ffffff',
    navigationBarTitleText: '筏筏',
  },
  tabBar: {
    // custom: true,
    selectedColor: 'FFA505',
    list: [
      {
        pagePath: 'pages/index/index',
        text: '首页',
        iconPath: './assets/images/index.png',
        selectedIconPath: './assets/images/index.png'
      },
      {
        pagePath: 'pages/mine/index',
        text: '我的',
        iconPath: './assets/images/mine.png',
        selectedIconPath: './assets/images/mine.png'
      }
    ]
  },
  usingComponents: Object.assign(useGlobalIconFont()),
}
