// demo img resources path
const d_1 = require('../assets/images/demo1.jpg');
const d_2 = require('../assets/images/demo2.jpg');
const d_3 = require('../assets/images/demo3.jpg');
const d_4 = require('../assets/images/demo4.jpg');

export {
  d_1,
  d_2,
  d_3,
  d_4
}