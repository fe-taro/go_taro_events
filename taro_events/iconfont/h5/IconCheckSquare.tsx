/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconCheckSquare: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M475.733333 715.776c-13.44 0-26.922667-4.992-35.370666-14.976l-234.24-234.538667a49.237333 49.237333 0 0 1 0-69.888 50.773333 50.773333 0 0 1 70.826666 0l197.12 197.973334 421.333334-417.536c20.224-19.968 52.224-19.968 70.784 0 20.224 19.968 20.224 51.584 0 69.888L511.146667 700.8c-8.405333 9.984-21.888 14.933333-35.413334 14.933333z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <path
        d="M856.618667 938.666667H93.226667C64.554667 938.666667 42.666667 917.034667 42.666667 888.746667V135.253333C42.666667 106.965333 64.554667 85.333333 93.226667 85.333333h672.426666c28.586667 0 50.517333 21.632 50.517334 49.92 0 28.245333-21.888 49.92-50.56 49.92H143.786667v653.653334h662.272v-325.973334c0-28.288 21.930667-49.92 50.56-49.92 28.672 0 50.56 21.632 50.56 49.92v377.6c0 26.581333-21.888 48.213333-50.56 48.213334z"
        fill={getIconColor(color, 1, '#333333')}
      />
    </svg>
  );
};

IconCheckSquare.defaultProps = {
  size: 18,
};

export default IconCheckSquare;
