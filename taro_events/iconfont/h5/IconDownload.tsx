/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconDownload: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M512 741.973333c-12.501333 0-23.466667-4.693333-32.853333-14.08l-281.6-281.6c-18.773333-18.773333-18.773333-48.512 0-65.706666 18.773333-18.773333 48.512-18.773333 65.706666 0L512 629.333333l248.746667-248.746666c18.773333-18.773333 48.512-18.773333 65.706666 0 18.773333 18.773333 18.773333 48.512 0 65.706666l-281.6 281.6c-9.386667 10.965333-20.352 14.08-32.853333 14.08z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <path
        d="M934.4 981.333333H89.6A46.08 46.08 0 0 1 42.666667 934.4v-397.354667c0-26.624 20.352-46.933333 46.933333-46.933333 26.581333 0 46.933333 20.309333 46.933333 46.933333V887.466667h750.933334v-350.421334c0-26.624 20.352-46.933333 46.933333-46.933333 26.581333 0 46.933333 20.309333 46.933333 46.933333V934.4c0 26.581333-20.352 46.933333-46.933333 46.933333z"
        fill={getIconColor(color, 1, '#333333')}
      />
      <path
        d="M512 691.925333a46.08 46.08 0 0 1-46.933333-46.933333V89.6c0-26.581333 20.352-46.933333 46.933333-46.933333 26.581333 0 46.933333 20.352 46.933333 46.933333v555.392c0 26.581333-20.352 46.933333-46.933333 46.933333z"
        fill={getIconColor(color, 2, '#333333')}
      />
    </svg>
  );
};

IconDownload.defaultProps = {
  size: 18,
};

export default IconDownload;
