/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconJingyin: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M602.66 936.26a140.66 140.66 0 0 1-45.18-7.52l-1.14-0.38-1.1-0.5-185.56-82.74a87.44 87.44 0 0 0-26.96-4.26h-162a139.54 139.54 0 0 1-139.38-139.38v-305.8a139.54 139.54 0 0 1 139.38-139.38h162a86.88 86.88 0 0 0 26.96-4.3l186.66-83.2 1.14-0.4a139.38 139.38 0 0 1 184.12 132v496.32a139.7 139.7 0 0 1-138.94 139.54z m-27.42-56.42a87.38 87.38 0 0 0 114.36-83.12V300.42a87.38 87.38 0 0 0-114.36-83.12l-186.66 83.24-1.14 0.38a138.86 138.86 0 0 1-44.72 7.38h-162a87.48 87.48 0 0 0-87.38 87.38v305.8a87.48 87.48 0 0 0 87.38 87.38h162a139.38 139.38 0 0 1 44.72 7.36l1.14 0.4 1.1 0.48z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M957.18 752a26 26 0 0 1-23.38-14.62l-172.94-354.84a26 26 0 0 1 46.76-22.78l172.92 354.88A26 26 0 0 1 957.18 752z"
        fill={getIconColor(color, 1, '#2AB1E8')}
      />
      <path
        d="M957.18 752a26 26 0 0 1-23.38-14.62l-172.94-354.84a26 26 0 0 1 46.76-22.78l172.92 354.88A26 26 0 0 1 957.18 752z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
      <path
        d="M792.88 752a26 26 0 0 1-23.36-37.4l172.94-354.88a26 26 0 1 1 46.74 22.78l-172.92 354.92a26 26 0 0 1-23.4 14.58z"
        fill={getIconColor(color, 3, '#F6AE65')}
      />
    </svg>
  );
};

IconJingyin.defaultProps = {
  size: 18,
};

export default IconJingyin;
