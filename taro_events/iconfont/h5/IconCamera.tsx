/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconCamera: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M934.4 853.333333H89.6A46.08 46.08 0 0 1 42.666667 806.485333V253.952a46.08 46.08 0 0 1 46.933333-46.848h211.2l10.965333-81.194667c3.114667-23.424 23.466667-40.576 46.933334-40.576h309.76c23.466667 0 43.776 17.152 46.933333 40.576l10.922667 81.194667H934.4c26.581333 0 46.933333 20.266667 46.933333 46.805333v551.04c0 26.538667-20.352 48.384-46.933333 48.384zM136.533333 759.68h750.933334V300.757333h-204.928c-23.466667 0-43.818667-17.194667-46.933334-40.576l-9.386666-81.194666H399.36l-10.965333 81.194666c-3.114667 23.381333-23.466667 40.533333-46.933334 40.533334H136.533333v458.965333z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <path
        d="M512 715.946667c-106.368 0-194.005333-87.381333-194.005333-193.536 0-106.154667 87.637333-193.578667 194.005333-193.578667s194.005333 87.466667 194.005333 193.578667A193.706667 193.706667 0 0 1 512 715.946667z m0-293.461334a100.693333 100.693333 0 0 0-100.138667 99.925334A100.693333 100.693333 0 0 0 512 622.293333a100.693333 100.693333 0 0 0 100.138667-99.882666A99.712 99.712 0 0 0 512 422.485333z"
        fill={getIconColor(color, 1, '#333333')}
      />
    </svg>
  );
};

IconCamera.defaultProps = {
  size: 18,
};

export default IconCamera;
