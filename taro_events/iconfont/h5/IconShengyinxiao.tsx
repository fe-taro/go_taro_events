/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconShengyinxiao: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M634.2 936.26a140.66 140.66 0 0 1-45.18-7.52l-1.16-0.38-1.1-0.5-185.54-82.74a87.58 87.58 0 0 0-26.98-4.26h-161.88a139.56 139.56 0 0 1-139.4-139.38v-305.8a139.54 139.54 0 0 1 139.4-139.38h161.88a87 87 0 0 0 26.98-4.3l186.64-83.2 1.16-0.4a139.38 139.38 0 0 1 184.1 132v496.32a139.7 139.7 0 0 1-138.92 139.54z m-27.42-56.42a87.38 87.38 0 0 0 114.34-83.12V300.42a87.38 87.38 0 0 0-114.34-83.12l-186.66 83.24-1.14 0.38a138.98 138.98 0 0 1-44.74 7.38h-161.88a87.48 87.48 0 0 0-87.4 87.38v305.8a87.48 87.48 0 0 0 87.4 87.38h161.88a139.5 139.5 0 0 1 44.74 7.36l1.14 0.4 1.1 0.48z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M839.02 736.92a26 26 0 0 1-16.72-46c67.7-56.74 99.82-112.54 95.7-165.78-6.32-79.14-91.46-131.14-92.32-131.68a26 26 0 0 1 26.62-44.66c4.44 2.64 108.82 65.88 117.44 171.86 5.76 71.08-32.58 141.8-114 210.16a26 26 0 0 1-16.72 6.1z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </svg>
  );
};

IconShengyinxiao.defaultProps = {
  size: 18,
};

export default IconShengyinxiao;
