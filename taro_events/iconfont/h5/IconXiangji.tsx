/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconXiangji: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M851.46 872.22H197.16A139.24 139.24 0 0 1 58 733.16V384.62a139.24 139.24 0 0 1 139.08-139.08h90.72c2-3.02 4.32-11.28 6-16.9 3.64-12.52 8.16-28.12 18.4-42l1.82-2.46a139.58 139.58 0 0 1 112-56.6h181.3a139.4 139.4 0 0 1 113.1 58.14c10 14 14.48 29.4 18.14 41.88 1.76 6 4.4 15.02 6.48 18a9.56 9.56 0 0 0 1.24 0h105.26a139.24 139.24 0 0 1 139.06 139.08v348.48a139.22 139.22 0 0 1-139.14 139.06zM197.16 296.94a87.78 87.78 0 0 0-87.68 87.68v348.54a87.78 87.78 0 0 0 87.68 87.68h654.3a87.78 87.78 0 0 0 87.68-87.68V384.62a87.78 87.78 0 0 0-87.68-87.68H746.2c-40.94 0-51.02-34.4-57.04-54.94-2.96-10-5.74-19.6-10.62-26.4a87.84 87.84 0 0 0-71.3-36.66H426a88 88 0 0 0-70.6 35.68l-1.8 2.46c-4.82 6.54-7.54 16-10.42 25.86-5.86 20.2-15.68 54-56.16 54z m91.4-51.52z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M524.3 723.06A169.1 169.1 0 1 1 693.4 554a169.28 169.28 0 0 1-169.1 169.06z m0-286.8A117.7 117.7 0 1 0 642 554a117.84 117.84 0 0 0-117.7-117.74z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <path
        d="M706.76 361.72m23.28 0l129.16 0q23.28 0 23.28 23.28l0 8.3q0 23.28-23.28 23.28l-129.16 0q-23.28 0-23.28-23.28l0-8.3q0-23.28 23.28-23.28Z"
        fill={getIconColor(color, 2, '#231815')}
      />
    </svg>
  );
};

IconXiangji.defaultProps = {
  size: 18,
};

export default IconXiangji;
