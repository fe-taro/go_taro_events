/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconRili: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M828.36 883.22H234a139.54 139.54 0 0 1-139.4-139.38v-422.4A139.54 139.54 0 0 1 234 182h594.36a139.54 139.54 0 0 1 139.38 139.38v422.4a139.54 139.54 0 0 1-139.38 139.44zM234 234a87.48 87.48 0 0 0-87.4 87.38v422.4a87.48 87.48 0 0 0 87.4 87.44h594.36a87.48 87.48 0 0 0 87.38-87.38v-422.4A87.48 87.48 0 0 0 828.36 234z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M941.74 475.04H120.66a26 26 0 0 1 0-52h821.08a26 26 0 0 1 0 52z"
        fill={getIconColor(color, 1, '#231815')}
      />
      <path
        d="M321.78 119.14m39.68 0l0.02 0q39.68 0 39.68 39.68l0 135.16q0 39.68-39.68 39.68l-0.02 0q-39.68 0-39.68-39.68l0-135.16q0-39.68 39.68-39.68Z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
      <path
        d="M644.76 119.14m39.68 0l0.02 0q39.68 0 39.68 39.68l0 135.16q0 39.68-39.68 39.68l-0.02 0q-39.68 0-39.68-39.68l0-135.16q0-39.68 39.68-39.68Z"
        fill={getIconColor(color, 3, '#F6AE65')}
      />
      <path
        d="M361.46 542.18m24 0l39.44 0q24 0 24 24l0 39.44q0 24-24 24l-39.44 0q-24 0-24-24l0-39.44q0-24 24-24Z"
        fill={getIconColor(color, 4, '#F6AE65')}
      />
      <path
        d="M640.72 542.18m24 0l39.44 0q24 0 24 24l0 39.44q0 24-24 24l-39.44 0q-24 0-24-24l0-39.44q0-24 24-24Z"
        fill={getIconColor(color, 5, '#F6AE65')}
      />
      <path
        d="M361.46 680.34m24 0l39.44 0q24 0 24 24l0 39.44q0 24-24 24l-39.44 0q-24 0-24-24l0-39.44q0-24 24-24Z"
        fill={getIconColor(color, 6, '#F6AE65')}
      />
      <path
        d="M640.72 680.34m24 0l39.44 0q24 0 24 24l0 39.44q0 24-24 24l-39.44 0q-24 0-24-24l0-39.44q0-24 24-24Z"
        fill={getIconColor(color, 7, '#F6AE65')}
      />
    </svg>
  );
};

IconRili.defaultProps = {
  size: 18,
};

export default IconRili;
