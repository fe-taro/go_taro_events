/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconChevronRight: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M351.36 896c-14.08 0-26.368-5.12-36.906667-15.274667-21.034667-20.394667-21.034667-52.693333 0-71.338666l284.458667-275.2L314.453333 257.28c-21.034667-20.352-21.034667-52.650667 0-71.338667 21.077333-20.352 54.442667-20.352 73.770667 0l321.28 312.576c21.077333 20.394667 21.077333 52.650667 0 71.338667L388.266667 880.725333a51.370667 51.370667 0 0 1-36.864 15.274667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconChevronRight.defaultProps = {
  size: 18,
};

export default IconChevronRight;
