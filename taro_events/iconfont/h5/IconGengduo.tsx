/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconGengduo: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M350.92 481.78h-65.42a139.54 139.54 0 0 1-139.4-139.38v-65.44a139.54 139.54 0 0 1 139.4-139.38h65.42a139.54 139.54 0 0 1 139.38 139.38v65.44a139.54 139.54 0 0 1-139.38 139.38z m-65.42-292.2a87.48 87.48 0 0 0-87.4 87.38v65.44a87.48 87.48 0 0 0 87.4 87.38h65.42a87.48 87.48 0 0 0 87.38-87.38v-65.44a87.48 87.48 0 0 0-87.38-87.38z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M725.16 478.64h-65.44a139.54 139.54 0 0 1-139.38-139.38v-65.42a139.54 139.54 0 0 1 139.38-139.4h65.44a139.54 139.54 0 0 1 139.38 139.4v65.42a139.54 139.54 0 0 1-139.38 139.38z m-65.44-292.2a87.48 87.48 0 0 0-87.38 87.4v65.42a87.48 87.48 0 0 0 87.38 87.38h65.44a87.48 87.48 0 0 0 87.38-87.38v-65.42a87.48 87.48 0 0 0-87.38-87.4z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <path
        d="M350.92 849.38h-65.42A139.56 139.56 0 0 1 146.1 710v-65.44a139.54 139.54 0 0 1 139.4-139.38h65.42a139.54 139.54 0 0 1 139.38 139.38V710a139.54 139.54 0 0 1-139.38 139.38z m-65.42-292.2a87.48 87.48 0 0 0-87.4 87.38V710a87.48 87.48 0 0 0 87.4 87.38h65.42A87.48 87.48 0 0 0 438.3 710v-65.44a87.48 87.48 0 0 0-87.38-87.38zM725.16 846.24h-65.44a139.54 139.54 0 0 1-139.38-139.38v-65.44A139.54 139.54 0 0 1 659.72 502h65.44a139.54 139.54 0 0 1 139.38 139.38v65.44a139.54 139.54 0 0 1-139.38 139.42zM659.72 554a87.48 87.48 0 0 0-87.38 87.38v65.44a87.48 87.48 0 0 0 87.38 87.38h65.44a87.48 87.48 0 0 0 87.38-87.38v-65.4A87.48 87.48 0 0 0 725.16 554z"
        fill={getIconColor(color, 2, '#231815')}
      />
    </svg>
  );
};

IconGengduo.defaultProps = {
  size: 18,
};

export default IconGengduo;
