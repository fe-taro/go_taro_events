/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconBluetooth: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M549.930667 981.333333c-26.88 0-47.402667-20.352-47.402667-46.933333V89.6c0-26.581333 20.522667-46.933333 47.36-46.933333 26.88 0 47.445333 20.352 47.445333 46.933333v844.8c0 26.581333-22.101333 46.933333-47.402666 46.933333z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <path
        d="M260.736 845.226667c-12.629333 0-23.68-4.693333-33.194667-14.08-17.365333-17.194667-17.365333-46.933333 0-65.706667L698.453333 299.221333l-181.717333-178.346666c-18.944-17.194667-18.944-46.933333 0-65.706667 17.365333-18.773333 47.402667-18.773333 66.389333 0l214.912 212.778667c9.472 9.386667 14.208 20.352 14.208 32.853333 0 12.501333-4.736 23.466667-14.208 32.853333L293.930667 832.725333a54.698667 54.698667 0 0 1-33.194667 12.501334z"
        fill={getIconColor(color, 1, '#333333')}
      />
      <path
        d="M549.930667 981.333333c-12.629333 0-23.722667-4.693333-33.194667-14.08a47.914667 47.914667 0 0 1 0-65.706666l181.76-179.925334L227.498667 256.981333c-17.365333-17.194667-17.365333-46.933333 0-65.706666 17.408-18.773333 47.402667-17.194667 66.389333 0L799.573333 691.925333c9.472 9.386667 14.208 20.309333 14.208 32.853334 0 12.501333-4.736 23.466667-14.208 32.853333l-218.069333 211.2c-7.936 9.386667-20.565333 12.501333-31.616 12.501333z"
        fill={getIconColor(color, 2, '#333333')}
      />
    </svg>
  );
};

IconBluetooth.defaultProps = {
  size: 18,
};

export default IconBluetooth;
