/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconTianjia: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M515.4 954A448 448 0 0 1 198.56 189.1a448 448 0 1 1 633.68 633.68A445.14 445.14 0 0 1 515.4 954z m0-844.14c-218.4 0-396 177.66-396 396s177.68 396 396 396 396-177.68 396-396-177.6-395.98-396-395.98z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M487.06 212.12m24 0l8.7 0q24 0 24 24l0 539.62q0 24-24 24l-8.7 0q-24 0-24-24l0-539.62q0-24 24-24Z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <path
        d="M809.22 476.22m0 24l0 8.7q0 24-24 24l-539.62 0q-24 0-24-24l0-8.7q0-24 24-24l539.62 0q24 0 24 24Z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
    </svg>
  );
};

IconTianjia.defaultProps = {
  size: 18,
};

export default IconTianjia;
