/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconJilu: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M786 945.8H262.64a139.54 139.54 0 0 1-139.4-139.38V217.58a139.54 139.54 0 0 1 139.4-139.38H786a139.54 139.54 0 0 1 139.38 139.38v588.84A139.54 139.54 0 0 1 786 945.8zM262.64 130.2a87.48 87.48 0 0 0-87.4 87.38v588.84a87.48 87.48 0 0 0 87.4 87.38H786a87.48 87.48 0 0 0 87.38-87.38V217.58A87.48 87.48 0 0 0 786 130.2z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M279.2 278.2m28.34 0l209.96 0q28.34 0 28.34 28.34l0 0.02q0 28.34-28.34 28.34l-209.96 0q-28.34 0-28.34-28.34l0-0.02q0-28.34 28.34-28.34Z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <path
        d="M279.2 504.3m28.34 0l410.32 0q28.34 0 28.34 28.34l0 0.02q0 28.34-28.34 28.34l-410.32 0q-28.34 0-28.34-28.34l0-0.02q0-28.34 28.34-28.34Z"
        fill={getIconColor(color, 2, '#231815')}
      />
      <path
        d="M279.2 695.72m28.34 0l410.32 0q28.34 0 28.34 28.34l0 0.02q0 28.34-28.34 28.34l-410.32 0q-28.34 0-28.34-28.34l0-0.02q0-28.34 28.34-28.34Z"
        fill={getIconColor(color, 3, '#231815')}
      />
    </svg>
  );
};

IconJilu.defaultProps = {
  size: 18,
};

export default IconJilu;
