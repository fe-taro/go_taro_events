/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconSousuo: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M468.1 886c-225.52 0-408.98-183.5-408.98-409.02S242.58 68 468.1 68s408.98 183.46 408.98 408.98S693.62 886 468.1 886z m0-766c-196.84 0-356.98 160.14-356.98 356.98S271.26 834 468.1 834s356.98-160.14 356.98-356.98S664.94 120 468.1 120z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M217.36 636.7A26 26 0 0 1 194 622.14a311.58 311.58 0 0 1 142.44-416.98 26 26 0 1 1 22.9 46.68c-128.48 63.06-181.72 218.9-118.66 347.4a26 26 0 0 1-23.32 37.46z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <path
        d="M703.718732 766.595201m20.039406-20.039406l0.014142-0.014142q20.039406-20.039406 40.078812 0l150.104628 150.104627q20.039406 20.039406 0 40.078813l-0.014142 0.014142q-20.039406 20.039406-40.078813 0l-150.104627-150.104628q-20.039406-20.039406 0-40.078812Z"
        fill={getIconColor(color, 2, '#231815')}
      />
    </svg>
  );
};

IconSousuo.defaultProps = {
  size: 18,
};

export default IconSousuo;
