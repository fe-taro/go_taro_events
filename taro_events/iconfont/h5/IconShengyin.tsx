/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconShengyin: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M569.06 936.28a140.66 140.66 0 0 1-45.18-7.52l-1.14-0.38-1.1-0.5L336 845.14a87.44 87.44 0 0 0-26.96-4.26H147.22a139.54 139.54 0 0 1-139.38-139.38v-305.8a139.54 139.54 0 0 1 139.38-139.38h162A86.88 86.88 0 0 0 336 252l186.66-83.22 1.14-0.4a139.38 139.38 0 0 1 184.12 132v496.36a139.7 139.7 0 0 1-138.94 139.54z m-27.42-56.42A87.38 87.38 0 0 0 656 796.74V300.44a87.38 87.38 0 0 0-114.36-83.12l-186.66 83.24-1.14 0.38a138.86 138.86 0 0 1-44.72 7.38H147.22a87.48 87.48 0 0 0-87.38 87.38v305.8a87.48 87.48 0 0 0 87.38 87.38h162a139.38 139.38 0 0 1 44.72 7.36l1.14 0.4 1.1 0.48z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M754 736.94a26 26 0 0 1-16.74-46c67.78-56.92 100-112.8 95.62-166.14-3.32-40.8-28-74.2-48-95.04a244.94 244.94 0 0 0-44.12-36.32 26 26 0 1 1 26.58-44.7c4.44 2.64 108.82 65.86 117.42 171.86 5.78 71.08-32.58 141.8-114 210.16a25.82 25.82 0 0 1-16.76 6.18z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <path
        d="M869.78 770.96a28.32 28.32 0 0 1-20-48.38c69.52-69.3 102-138 96.32-204.12-8-95.16-91.82-155.28-92.66-155.88a28.34 28.34 0 0 1 32.66-46.3c4.32 3.06 106.18 76.24 116.44 197.4 7.1 83.6-30.86 167.38-112.78 249.02a28.26 28.26 0 0 1-19.98 8.26z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
    </svg>
  );
};

IconShengyin.defaultProps = {
  size: 18,
};

export default IconShengyin;
