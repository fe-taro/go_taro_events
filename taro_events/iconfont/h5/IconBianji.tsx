/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconBianji: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M285.68 779.24a48.82 48.82 0 0 1-48.56-53.44l10.18-107.4a83.08 83.08 0 0 1 23.88-50.78L668.86 170A82.94 82.94 0 0 1 786 170l60.16 60.16a82.8 82.8 0 0 1 0 117.16L448.5 744.94a83.04 83.04 0 0 1-50.76 23.9l-107.4 10.18c-1.56 0.14-3.12 0.22-4.66 0.22z m109.6-36.44zM727.44 198a30.44 30.44 0 0 0-21.6 8.92L308.16 604.6a30.64 30.64 0 0 0-8.8 18.72l-9.78 103.24 103.22-9.78a30.62 30.62 0 0 0 18.72-8.82L809.2 310.28a30.52 30.52 0 0 0 0-43.2l-60.16-60.16a30.36 30.36 0 0 0-21.6-8.92z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M207.88 824.22m28.34 0l657.66 0q28.34 0 28.34 28.34l0 0.02q0 28.34-28.34 28.34l-657.66 0q-28.34 0-28.34-28.34l0-0.02q0-28.34 28.34-28.34Z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </svg>
  );
};

IconBianji.defaultProps = {
  size: 18,
};

export default IconBianji;
