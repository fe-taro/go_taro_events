/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconCirclecheckfull: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M512 42.666667C252.288 42.666667 42.666667 252.288 42.666667 512s209.621333 469.333333 469.333333 469.333333 469.333333-209.621333 469.333333-469.333333S771.712 42.666667 512 42.666667z m228.394667 372.352l-258.133334 259.669333c-9.386667 9.386667-20.309333 14.08-32.853333 14.08-12.501333 0-25.002667-4.693333-32.853333-14.08l-131.413334-134.528a46.72 46.72 0 0 1 0-65.706667 46.72 46.72 0 0 1 65.706667 0l98.56 100.138667 223.744-226.858667c18.773333-18.773333 48.469333-18.773333 65.706667 0 18.773333 18.773333 20.309333 48.512 1.536 67.285334z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconCirclecheckfull.defaultProps = {
  size: 18,
};

export default IconCirclecheckfull;
