/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconBox: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M533.333333 981.333333c-7.68 0-15.36-1.621333-23.04-6.314666l-359.253333-211.2A46.848 46.848 0 0 1 128 723.2v-422.4c0-17.194667 9.216-32.853333 23.04-40.661333l359.253333-211.2a48.128 48.128 0 0 1 46.08 0l359.253334 211.2c13.824 7.808 23.04 23.466667 23.04 40.661333v422.4c0 17.194667-9.216 32.853333-23.04 40.661333l-359.253334 211.2c-7.68 4.693333-15.36 6.272-23.04 6.272z m-313.173333-284.757333l313.173333 184.576 313.173334-184.576V327.338667l-313.173334-183.04-313.173333 183.04v369.237333z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <path
        d="M533.333333 576.128a42.666667 42.666667 0 0 1-23.04-6.229333l-359.253333-211.2c-21.504-12.544-29.184-42.24-16.896-64.170667A46.506667 46.506667 0 0 1 197.077333 277.333333l336.256 197.12 336.213334-197.12a46.506667 46.506667 0 0 1 62.976 17.194667c12.288 21.930667 4.608 51.626667-16.896 64.170667l-359.253334 211.2c-7.68 3.114667-15.36 6.229333-23.04 6.229333z"
        fill={getIconColor(color, 1, '#333333')}
      />
      <path
        d="M533.333333 981.333333c-26.112 0-46.08-20.352-46.08-46.933333v-406.741333c0-26.624 19.968-46.933333 46.08-46.933334 26.112 0 46.08 20.309333 46.08 46.933334V934.4c0 26.581333-19.968 46.933333-46.08 46.933333z"
        fill={getIconColor(color, 2, '#333333')}
      />
    </svg>
  );
};

IconBox.defaultProps = {
  size: 18,
};

export default IconBox;
