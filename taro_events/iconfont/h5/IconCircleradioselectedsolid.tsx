/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconCircleradioselectedsolid: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M512 42.666667C252.288 42.666667 42.666667 252.288 42.666667 512s209.621333 469.333333 469.333333 469.333333 469.333333-209.621333 469.333333-469.333333S771.712 42.666667 512 42.666667z m0 611.712A142.890667 142.890667 0 0 1 369.621333 512 142.890667 142.890667 0 0 1 512 369.621333 142.890667 142.890667 0 0 1 654.378667 512 142.890667 142.890667 0 0 1 512 654.378667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconCircleradioselectedsolid.defaultProps = {
  size: 18,
};

export default IconCircleradioselectedsolid;
