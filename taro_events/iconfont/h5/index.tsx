/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import IconBookmark from './IconBookmark';
import IconBluetooth from './IconBluetooth';
import IconBox from './IconBox';
import IconBookOpen from './IconBookOpen';
import IconAnnouncementspeaker from './IconAnnouncementspeaker';
import IconCheck from './IconCheck';
import IconCamera from './IconCamera';
import IconChevronLeft from './IconChevronLeft';
import IconChevronDown from './IconChevronDown';
import IconChevronRight from './IconChevronRight';
import IconCheckSquare from './IconCheckSquare';
import IconCheckCircle from './IconCheckCircle';
import IconChevronsRight from './IconChevronsRight';
import IconChevronsLeft from './IconChevronsLeft';
import IconChevronUp from './IconChevronUp';
import IconCirclecheckfull from './IconCirclecheckfull';
import IconCircleradioselectedsolid from './IconCircleradioselectedsolid';
import IconCircle from './IconCircle';
import IconClock from './IconClock';
import IconCode from './IconCode';
import IconDownload from './IconDownload';
import IconBianji from './IconBianji';
import IconCuowu from './IconCuowu';
import IconDianzan from './IconDianzan';
import IconJiazai from './IconJiazai';
import IconSaoyisao from './IconSaoyisao';
import IconLuxiang from './IconLuxiang';
import IconFasong from './IconFasong';
import IconShengyin from './IconShengyin';
import IconXiaoxi from './IconXiaoxi';
import IconShoucangjia from './IconShoucangjia';
import IconWode from './IconWode';
import IconDingwei from './IconDingwei';
import IconJingyin from './IconJingyin';
import IconQiandaochenggong from './IconQiandaochenggong';
import IconShouye from './IconShouye';
import IconYouhuiquan from './IconYouhuiquan';
import IconShoucang2 from './IconShoucang2';
import IconShengyinxiao from './IconShengyinxiao';
import IconRili from './IconRili';
import IconShuqian from './IconShuqian';
import IconJilu from './IconJilu';
import IconXiangji from './IconXiangji';
import IconShezhi from './IconShezhi';
import IconFaxian from './IconFaxian';
import IconXinxi from './IconXinxi';
import IconTianjia from './IconTianjia';
import IconShexiang from './IconShexiang';
import IconGengduo from './IconGengduo';
import IconShoucang from './IconShoucang';
import IconSousuo from './IconSousuo';

export type IconNames = 'bookmark' | 'bluetooth' | 'box' | 'book-open' | 'announcementspeaker' | 'check' | 'camera' | 'chevron-left' | 'chevron-down' | 'chevron-right' | 'check-square' | 'check-circle' | 'chevrons-right' | 'chevrons-left' | 'chevron-up' | 'circlecheckfull' | 'circleradioselectedsolid' | 'circle' | 'clock' | 'code' | 'download' | 'bianji' | 'cuowu' | 'dianzan' | 'jiazai' | 'saoyisao' | 'luxiang' | 'fasong' | 'shengyin' | 'xiaoxi' | 'shoucangjia' | 'wode' | 'dingwei' | 'jingyin' | 'qiandaochenggong' | 'shouye' | 'youhuiquan' | 'shoucang2' | 'shengyinxiao' | 'rili' | 'shuqian' | 'jilu' | 'xiangji' | 'shezhi' | 'faxian' | 'xinxi' | 'tianjia' | 'shexiang' | 'gengduo' | 'shoucang' | 'sousuo';

interface Props extends DOMAttributes<SVGElement> {
  name: IconNames;
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const IconFont: FunctionComponent<Props> = ({ name, ...rest }) => {
  switch (name) {
    case 'bookmark':
      return <IconBookmark {...rest} />;
    case 'bluetooth':
      return <IconBluetooth {...rest} />;
    case 'box':
      return <IconBox {...rest} />;
    case 'book-open':
      return <IconBookOpen {...rest} />;
    case 'announcementspeaker':
      return <IconAnnouncementspeaker {...rest} />;
    case 'check':
      return <IconCheck {...rest} />;
    case 'camera':
      return <IconCamera {...rest} />;
    case 'chevron-left':
      return <IconChevronLeft {...rest} />;
    case 'chevron-down':
      return <IconChevronDown {...rest} />;
    case 'chevron-right':
      return <IconChevronRight {...rest} />;
    case 'check-square':
      return <IconCheckSquare {...rest} />;
    case 'check-circle':
      return <IconCheckCircle {...rest} />;
    case 'chevrons-right':
      return <IconChevronsRight {...rest} />;
    case 'chevrons-left':
      return <IconChevronsLeft {...rest} />;
    case 'chevron-up':
      return <IconChevronUp {...rest} />;
    case 'circlecheckfull':
      return <IconCirclecheckfull {...rest} />;
    case 'circleradioselectedsolid':
      return <IconCircleradioselectedsolid {...rest} />;
    case 'circle':
      return <IconCircle {...rest} />;
    case 'clock':
      return <IconClock {...rest} />;
    case 'code':
      return <IconCode {...rest} />;
    case 'download':
      return <IconDownload {...rest} />;
    case 'bianji':
      return <IconBianji {...rest} />;
    case 'cuowu':
      return <IconCuowu {...rest} />;
    case 'dianzan':
      return <IconDianzan {...rest} />;
    case 'jiazai':
      return <IconJiazai {...rest} />;
    case 'saoyisao':
      return <IconSaoyisao {...rest} />;
    case 'luxiang':
      return <IconLuxiang {...rest} />;
    case 'fasong':
      return <IconFasong {...rest} />;
    case 'shengyin':
      return <IconShengyin {...rest} />;
    case 'xiaoxi':
      return <IconXiaoxi {...rest} />;
    case 'shoucangjia':
      return <IconShoucangjia {...rest} />;
    case 'wode':
      return <IconWode {...rest} />;
    case 'dingwei':
      return <IconDingwei {...rest} />;
    case 'jingyin':
      return <IconJingyin {...rest} />;
    case 'qiandaochenggong':
      return <IconQiandaochenggong {...rest} />;
    case 'shouye':
      return <IconShouye {...rest} />;
    case 'youhuiquan':
      return <IconYouhuiquan {...rest} />;
    case 'shoucang2':
      return <IconShoucang2 {...rest} />;
    case 'shengyinxiao':
      return <IconShengyinxiao {...rest} />;
    case 'rili':
      return <IconRili {...rest} />;
    case 'shuqian':
      return <IconShuqian {...rest} />;
    case 'jilu':
      return <IconJilu {...rest} />;
    case 'xiangji':
      return <IconXiangji {...rest} />;
    case 'shezhi':
      return <IconShezhi {...rest} />;
    case 'faxian':
      return <IconFaxian {...rest} />;
    case 'xinxi':
      return <IconXinxi {...rest} />;
    case 'tianjia':
      return <IconTianjia {...rest} />;
    case 'shexiang':
      return <IconShexiang {...rest} />;
    case 'gengduo':
      return <IconGengduo {...rest} />;
    case 'shoucang':
      return <IconShoucang {...rest} />;
    case 'sousuo':
      return <IconSousuo {...rest} />;

  }

  return null;
};

export default IconFont;
