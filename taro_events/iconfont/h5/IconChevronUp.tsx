/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconChevronUp: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M802.389333 725.333333a48.213333 48.213333 0 0 1-35.669333-15.744l-275.2-285.013333-276.906667 285.013333c-20.352 20.992-52.650667 20.992-71.338666 0-20.352-20.992-20.352-54.186667 0-73.429333l310.869333-321.749333c20.394667-20.992 52.650667-20.992 71.338667 0l312.576 321.706666c20.352 21.034667 20.352 54.229333 0 73.472a48.213333 48.213333 0 0 1-35.669334 15.744z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconChevronUp.defaultProps = {
  size: 18,
};

export default IconChevronUp;
