/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconShoucang2: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M313.24 883.52a129.72 129.72 0 0 1-62-16c-47.74-25.86-77.4-78-77.4-136V236.44c0-83.72 62.5-151.84 139.34-151.84h454c76.84 0 139.36 68.12 139.36 151.84v495.12c0 58-29.66 110.18-77.4 136-42.68 23.12-92.62 20.96-133.58-5.76l-112-73.12a78.16 78.16 0 0 0-86.52 0l-112 73.12a130.78 130.78 0 0 1-71.8 21.72z m226.96-160a130.74 130.74 0 0 1 71.64 21.62l112 73.12c25.12 16.38 54.44 17.68 80.48 3.58 30.96-16.76 50.2-51.4 50.2-90.36V236.44c0-55.08-39.22-99.9-87.42-99.9h-454c-48.2 0-87.42 44.82-87.42 100v495.02c0 38.96 19.24 73.6 50.22 90.36 26 14.1 55.34 12.8 80.46-3.58l112-73.12a130.66 130.66 0 0 1 71.84-21.62z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M472.28 532.62a51.38 51.38 0 0 1-50.7-58.82l7.78-52.76-39.04-36.32a51.3 51.3 0 0 1 26.36-88.12l52.58-8.9 22.48-48.36a51.28 51.28 0 0 1 92-2.14l24.7 47.26 52.94 6.42a51.28 51.28 0 0 1 30.46 86.8l-37.3 38 10.24 52.34A51.28 51.28 0 0 1 591.62 524l-47.78-23.8-46.62 26a51.16 51.16 0 0 1-24.94 6.42z m-45.48-184.98l38.26 35.58a51.3 51.3 0 0 1 15.8 45.04L473.24 480l45.66-25.4a51.3 51.3 0 0 1 47.72-1.12l46.8 23.24-10-51.28a51.22 51.22 0 0 1 13.68-45.74l36.54-37.32-51.88-6.38a51.22 51.22 0 0 1-39.26-27.14l-24.22-46.3-22 47.38a51.28 51.28 0 0 1-38 28.96z m188 129.72z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </svg>
  );
};

IconShoucang2.defaultProps = {
  size: 18,
};

export default IconShoucang2;
