/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconWode: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M791.86 919.74H239.1a139.54 139.54 0 0 1-139.38-139.4v-64.52a139.54 139.54 0 0 1 139.38-139.38h552.76a139.54 139.54 0 0 1 139.38 139.38v64.52a139.54 139.54 0 0 1-139.38 139.4zM239.1 628.44a87.48 87.48 0 0 0-87.38 87.38v64.52a87.48 87.48 0 0 0 87.38 87.4h552.76a87.48 87.48 0 0 0 87.38-87.4v-64.52a87.48 87.48 0 0 0-87.38-87.38z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M515.48 628.88c-153.48 0-278.44-124.88-278.44-278.44S362 72 515.48 72 794 196.92 794 350.44s-125 278.44-278.52 278.44z m0-504.86A226.44 226.44 0 1 0 742 350.44 226.68 226.68 0 0 0 515.48 124z"
        fill={getIconColor(color, 1, '#231815')}
      />
      <path
        d="M339.06 376.44a26 26 0 0 1-26-26A199.18 199.18 0 0 1 512 151.5a26 26 0 0 1 0 52 147.12 147.12 0 0 0-146.94 146.94 26 26 0 0 1-26 26z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
    </svg>
  );
};

IconWode.defaultProps = {
  size: 18,
};

export default IconWode;
