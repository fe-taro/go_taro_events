/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconDingwei: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M510.96 969.1l-14.36-10.56a1567.8 1567.8 0 0 1-183.28-163.08c-124.16-129.78-187.1-246-187.1-345.66 0-100.32 32.78-192.44 92.3-259.4C287.38 112.94 388.86 72 512 72c121.68 0 228.78 44.68 301.56 125.84 63.4 70.72 94 163.58 84 254.76-7.34 67.06-31.74 135.64-72.56 203.84-32.16 53.76-74.58 107.44-126 159.58-87.2 88.32-169.58 141.26-173.04 143.46zM512 124c-107.86 0-196 34.9-254.62 100.94-51.06 57.42-79.16 137.26-79.16 224.84 0 106.8 94 227.38 172.68 309.72a1571.36 1571.36 0 0 0 162 146 1115.48 1115.48 0 0 0 149.9-126.86c75.36-76.5 168.32-196.14 183.14-331.74 8.36-76.52-17.56-154.68-71.1-214.4C712 162.56 618.72 124 512 124z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M523.4 570c-46.22 0-90-19.7-123.28-55.48a187.16 187.16 0 0 1-50.16-126 173.46 173.46 0 0 1 346.9 0 187.16 187.16 0 0 1-50.16 126C613.42 550.34 569.64 570 523.4 570z m0-302.88A121.58 121.58 0 0 0 402 388.6c0 68.96 56.72 129.4 121.4 129.4s121.46-60.48 121.46-129.44a121.58 121.58 0 0 0-121.46-121.4z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </svg>
  );
};

IconDingwei.defaultProps = {
  size: 18,
};

export default IconDingwei;
