/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconCuowu: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M523.72 886.52c-221.3 0-401.34-180-401.34-401.34s180-401.34 401.34-401.34 401.34 180 401.34 401.34-180.04 401.34-401.34 401.34z m0-750.68c-192.62 0-349.34 156.72-349.34 349.34s156.72 349.34 349.34 349.34 349.34-156.7 349.34-349.34S716.34 135.84 523.72 135.84z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M718.78 697.22a26 26 0 0 1-17.58-6.84L311.06 332.1a26 26 0 0 1 35.18-38.32L736.36 652a26 26 0 0 1-17.58 45.14z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <path
        d="M374.58 697.38a26.14 26.14 0 0 1-19.48-43.56l332.18-372a26.14 26.14 0 1 1 39 34.82l-332.18 372a26 26 0 0 1-19.52 8.74z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
    </svg>
  );
};

IconCuowu.defaultProps = {
  size: 18,
};

export default IconCuowu;
