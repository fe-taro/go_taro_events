/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconXinxi: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M530.54 613.28a126.92 126.92 0 0 1-88-35.64L169.04 315.48a26 26 0 0 1 36-37.54l273.42 262.16a74.9 74.9 0 0 0 104.9-0.72l265.3-261.2a26 26 0 0 1 36.48 37.06l-265.3 261.2a126.9 126.9 0 0 1-89.3 36.84z"
        fill={getIconColor(color, 0, '#F6AE65')}
      />
      <path
        d="M839.42 858.78H226.44a139.54 139.54 0 0 1-139.38-139.38V263.74a139.54 139.54 0 0 1 139.38-139.4h612.98a139.56 139.56 0 0 1 139.4 139.4V719.4a139.56 139.56 0 0 1-139.4 139.38zM226.44 176.34a87.5 87.5 0 0 0-87.38 87.4V719.4a87.48 87.48 0 0 0 87.38 87.38h612.98a87.48 87.48 0 0 0 87.4-87.38V263.74a87.5 87.5 0 0 0-87.4-87.4z"
        fill={getIconColor(color, 1, '#231815')}
      />
    </svg>
  );
};

IconXinxi.defaultProps = {
  size: 18,
};

export default IconXinxi;
