/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, DOMAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends DOMAttributes<SVGElement> {
  size?: number;
  color?: string | string[];
  style?: CSSProperties;
  className?: string;
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'block',
};

const IconXiaoxi: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M838.96 858.72a80.86 80.86 0 0 1-52.18-19.36l-73.28-61.74a26.74 26.74 0 0 0-17.34-6.58H256c-77.44 0-140.44-67.28-140.44-150V328.14c0-82.7 63-150 140.44-150h526.4c77.46 0 140.46 67.28 140.46 150v442c0 35.64-19.18 66.7-50 81.06a80 80 0 0 1-33.9 7.52zM256 232.3c-47.58 0-86.3 43-86.3 95.84v292.92c0 52.86 38.72 95.84 86.3 95.84h440.16a81.3 81.3 0 0 1 52.24 19.32L821.68 798a25.3 25.3 0 0 0 28.2 4.14c9.08-4.22 18.76-14.82 18.76-32v-442c0-52.84-38.72-95.84-86.32-95.84z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <path
        d="M356.02 470.06m-53.32 0a53.32 53.32 0 1 0 106.64 0 53.32 53.32 0 1 0-106.64 0Z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <path
        d="M512 470.06m-53.32 0a53.32 53.32 0 1 0 106.64 0 53.32 53.32 0 1 0-106.64 0Z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
      <path
        d="M672.46 470.06m-53.32 0a53.32 53.32 0 1 0 106.64 0 53.32 53.32 0 1 0-106.64 0Z"
        fill={getIconColor(color, 3, '#F6AE65')}
      />
    </svg>
  );
};

IconXiaoxi.defaultProps = {
  size: 18,
};

export default IconXiaoxi;
