/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import Taro from '@tarojs/taro';
import Icon from './rn';

export type IconNames = 'bookmark' | 'bluetooth' | 'box' | 'book-open' | 'announcementspeaker' | 'check' | 'camera' | 'chevron-left' | 'chevron-down' | 'chevron-right' | 'check-square' | 'check-circle' | 'chevrons-right' | 'chevrons-left' | 'chevron-up' | 'circlecheckfull' | 'circleradioselectedsolid' | 'circle' | 'clock' | 'code' | 'download' | 'bianji' | 'cuowu' | 'dianzan' | 'jiazai' | 'saoyisao' | 'luxiang' | 'fasong' | 'shengyin' | 'xiaoxi' | 'shoucangjia' | 'wode' | 'dingwei' | 'jingyin' | 'qiandaochenggong' | 'shouye' | 'youhuiquan' | 'shoucang2' | 'shengyinxiao' | 'rili' | 'shuqian' | 'jilu' | 'xiangji' | 'shezhi' | 'faxian' | 'xinxi' | 'tianjia' | 'shexiang' | 'gengduo' | 'shoucang' | 'sousuo';

interface Props {
  name: IconNames;
  size?: number;
  color?: string | string[];
  style?: React.CSSProperties;
}

const IconFont: FunctionComponent<Props> = (props) => {
  const { name, size, color, style } = props;

  // @ts-ignore
  return <Icon name={name} size={parseFloat(Taro.pxTransform(size))} color={color} style={style} />;
};

IconFont.defaultProps = {
  size: 18,
};

export default IconFont;
