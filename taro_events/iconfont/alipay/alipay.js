Component({
  props: {
    // bookmark | bluetooth | box | book-open | announcementspeaker | check | camera | chevron-left | chevron-down | chevron-right | check-square | check-circle | chevrons-right | chevrons-left | chevron-up | circlecheckfull | circleradioselectedsolid | circle | clock | code | download | bianji | cuowu | dianzan | jiazai | saoyisao | luxiang | fasong | shengyin | xiaoxi | shoucangjia | wode | dingwei | jingyin | qiandaochenggong | shouye | youhuiquan | shoucang2 | shengyinxiao | rili | shuqian | jilu | xiangji | shezhi | faxian | xinxi | tianjia | shexiang | gengduo | shoucang | sousuo
    name: null,
    // string | string[]
    color: '',
    size: 18,
  },
  data: {
    quot: '"',
    svgSize: 18,
    isStr: true,
  },
  didMount() {
    const size = this.props.size;
    const color = this.props.color;

    this.setData({
      isStr: typeof color === 'string',
    });

    if (size !== this.data.svgSize) {
      this.setData({
        svgSize: size / 750 * my.getSystemInfoSync().windowWidth,
      });
    }
  },
  disUpdate(prevProps) {
    const size = this.props.size;
    const color = this.props.color;

    if (color !== prevProps.color) {
      this.setData({
        isStr: typeof color === 'string',
      });
    }

    if (size !== prevProps.size) {
      this.setData({
        svgSize: size / 750 * my.getSystemInfoSync().windowWidth,
      });
    }
  },
});
