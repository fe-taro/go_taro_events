Component({
  properties: {
    // bookmark | bluetooth | box | book-open | announcementspeaker | check | camera | chevron-left | chevron-down | chevron-right | check-square | check-circle | chevrons-right | chevrons-left | chevron-up | circlecheckfull | circleradioselectedsolid | circle | clock | code | download | bianji | cuowu | dianzan | jiazai | saoyisao | luxiang | fasong | shengyin | xiaoxi | shoucangjia | wode | dingwei | jingyin | qiandaochenggong | shouye | youhuiquan | shoucang2 | shengyinxiao | rili | shuqian | jilu | xiangji | shezhi | faxian | xinxi | tianjia | shexiang | gengduo | shoucang | sousuo
    name: {
      type: String,
    },
    // string | string[]
    color: {
      type: null,
      value: '',
      observer: function(color) {
        this.setData({
          isStr: typeof color === 'string',
        });
      }
    },
    size: {
      type: Number,
      value: 18,
      observer: function(size) {
        this.setData({
          svgSize: size / 750 * swan.getSystemInfoSync().windowWidth,
        });
      },
    },
  },
  data: {
    quot: '"',
    svgSize: 18 / 750 * swan.getSystemInfoSync().windowWidth,
    isStr: true,
  },
});
