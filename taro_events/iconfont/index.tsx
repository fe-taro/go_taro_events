/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';

export type IconNames = 'bookmark' | 'bluetooth' | 'box' | 'book-open' | 'announcementspeaker' | 'check' | 'camera' | 'chevron-left' | 'chevron-down' | 'chevron-right' | 'check-square' | 'check-circle' | 'chevrons-right' | 'chevrons-left' | 'chevron-up' | 'circlecheckfull' | 'circleradioselectedsolid' | 'circle' | 'clock' | 'code' | 'download' | 'bianji' | 'cuowu' | 'dianzan' | 'jiazai' | 'saoyisao' | 'luxiang' | 'fasong' | 'shengyin' | 'xiaoxi' | 'shoucangjia' | 'wode' | 'dingwei' | 'jingyin' | 'qiandaochenggong' | 'shouye' | 'youhuiquan' | 'shoucang2' | 'shengyinxiao' | 'rili' | 'shuqian' | 'jilu' | 'xiangji' | 'shezhi' | 'faxian' | 'xinxi' | 'tianjia' | 'shexiang' | 'gengduo' | 'shoucang' | 'sousuo';

export interface IconProps {
  name: IconNames;
  size?: number;
  color?: string | string[];
  style?: React.CSSProperties;
}

const IconFont: FunctionComponent<IconProps> = () => {
  return null;
};

export default IconFont;
