/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconChevronUp: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M802.389333 725.333333a48.213333 48.213333 0 0 1-35.669333-15.744l-275.2-285.013333-276.906667 285.013333c-20.352 20.992-52.650667 20.992-71.338666 0-20.352-20.992-20.352-54.186667 0-73.429333l310.869333-321.749333c20.394667-20.992 52.650667-20.992 71.338667 0l312.576 321.706666c20.352 21.034667 20.352 54.229333 0 73.472a48.213333 48.213333 0 0 1-35.669334 15.744z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </Svg>
  );
};

IconChevronUp.defaultProps = {
  size: 18,
};

export default IconChevronUp;
