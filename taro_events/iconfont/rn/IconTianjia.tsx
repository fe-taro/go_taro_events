/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconTianjia: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M515.4 954A448 448 0 0 1 198.56 189.1a448 448 0 1 1 633.68 633.68A445.14 445.14 0 0 1 515.4 954z m0-844.14c-218.4 0-396 177.66-396 396s177.68 396 396 396 396-177.68 396-396-177.6-395.98-396-395.98z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M487.06 212.12m24 0l8.7 0q24 0 24 24l0 539.62q0 24-24 24l-8.7 0q-24 0-24-24l0-539.62q0-24 24-24Z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
      <Path
        d="M809.22 476.22m0 24l0 8.7q0 24-24 24l-539.62 0q-24 0-24-24l0-8.7q0-24 24-24l539.62 0q24 0 24 24Z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
    </Svg>
  );
};

IconTianjia.defaultProps = {
  size: 18,
};

export default IconTianjia;
