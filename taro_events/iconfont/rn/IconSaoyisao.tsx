/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconSaoyisao: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M137.4 452.74a26 26 0 0 1-26-26V238a139.54 139.54 0 0 1 139.38-139.38h164.86a26 26 0 0 1 0 52h-164.86A87.48 87.48 0 0 0 163.4 238v188.84a26 26 0 0 1-26 25.9zM418 899.74h-167.22a139.54 139.54 0 0 1-139.38-139.4V606a26 26 0 0 1 52 0v154.3a87.48 87.48 0 0 0 87.38 87.4H418a26 26 0 1 1 0 52zM773.22 899.74h-120.96a26 26 0 0 1 0-52h120.96a87.48 87.48 0 0 0 87.38-87.4v-143.28a26 26 0 0 1 52 0v143.28a139.54 139.54 0 0 1-139.38 139.4zM886.6 442.74a26 26 0 0 1-26-26V238a87.48 87.48 0 0 0-87.38-87.38h-153.7a26 26 0 1 1 0-52h153.7A139.54 139.54 0 0 1 912.6 238v178.84a26 26 0 0 1-26 25.9z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M257.66 462.68m28.34 0l472.2 0q28.34 0 28.34 28.34l0 0.02q0 28.34-28.34 28.34l-472.2 0q-28.34 0-28.34-28.34l0-0.02q0-28.34 28.34-28.34Z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </Svg>
  );
};

IconSaoyisao.defaultProps = {
  size: 18,
};

export default IconSaoyisao;
