/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconBookmark: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M804.906667 981.333333c-12.885333 0-25.813333-4.693333-33.877334-14.08l-258.218666-251.861333-258.218667 251.861333c-14.506667 14.08-35.498667 18.773333-53.248 10.965334C181.973333 970.368 170.666667 953.173333 170.666667 934.4V180.352C170.666667 103.68 233.6 42.666667 312.704 42.666667h398.592C788.778667 42.666667 853.333333 103.68 853.333333 180.352V934.4c0 18.773333-11.306667 35.968-30.677333 43.818667-4.821333 3.114667-11.264 3.114667-17.749333 3.114666z m-293.717334-377.045333c12.928 0 25.813333 4.693333 33.877334 14.08l209.834666 203.392V180.352C756.48 155.306667 735.530667 136.533333 711.253333 136.533333H312.746667c-25.856 0-45.226667 18.773333-45.226667 43.818667V821.76l209.834667-203.392c9.685333-9.386667 20.992-14.08 33.877333-14.08z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </Svg>
  );
};

IconBookmark.defaultProps = {
  size: 18,
};

export default IconBookmark;
