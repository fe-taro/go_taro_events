/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconCode: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M375.893333 810.666667c-12.501333 0-23.466667-4.48-32.853333-13.482667l-286.293333-274.304c-18.773333-17.962667-18.773333-46.464 0-62.933333l286.293333-275.797334c18.773333-17.962667 48.512-17.962667 65.706667 0 18.773333 18.005333 18.773333 46.506667 0 62.976l-253.44 242.773334 253.44 242.816c18.773333 18.005333 18.773333 46.506667 0 62.976-7.808 10.496-20.352 14.976-32.853334 14.976zM648.106667 810.666667c-12.501333 0-23.466667-4.48-32.853334-13.482667-18.773333-18.005333-18.773333-46.506667 0-62.976l253.44-242.773333-255.018666-244.309334c-18.773333-18.005333-18.773333-46.506667 0-62.976 18.773333-17.962667 48.512-17.962667 65.706666 0l286.293334 275.797334c18.773333 17.962667 18.773333 46.464 0 62.933333l-284.714667 274.304c-9.386667 8.96-21.888 13.482667-32.853333 13.482667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </Svg>
  );
};

IconCode.defaultProps = {
  size: 18,
};

export default IconCode;
