/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconShouye: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M771.7 931H245.82c-76.58 0-138.9-60.44-138.9-134.72V479.36a133.16 133.16 0 0 1 46.14-100.28L416 150.28a142 142 0 0 1 185.52 0l262.94 228.8a133.16 133.16 0 0 1 46.14 100.28v316.92c0 74.28-62.32 134.72-138.9 134.72zM508.76 166.74a90 90 0 0 0-59.26 22L186.56 417.58A82.12 82.12 0 0 0 158 479.36v316.92C158 842.42 197.38 880 245.82 880h525.88c48.44 0 87.86-37.54 87.86-83.68V479.36a82.12 82.12 0 0 0-28.6-61.78L568 188.78a90.12 90.12 0 0 0-59.24-22.04z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M362.78 734.16m28.34 0l241.76 0q28.34 0 28.34 28.34l0 3.1q0 28.34-28.34 28.34l-241.76 0q-28.34 0-28.34-28.34l0-3.1q0-28.34 28.34-28.34Z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </Svg>
  );
};

IconShouye.defaultProps = {
  size: 18,
};

export default IconShouye;
