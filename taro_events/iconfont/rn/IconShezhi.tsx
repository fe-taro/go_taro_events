/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconShezhi: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M679.68 943.04H367.14a143.42 143.42 0 0 1-123.86-71.5L87.02 600.86a143.32 143.32 0 0 1 0-143l156.26-270.68a143.42 143.42 0 0 1 123.86-71.5h312.54a143.42 143.42 0 0 1 123.86 71.5l156.26 270.68a143.32 143.32 0 0 1 0 143l-156.26 270.68a143.42 143.42 0 0 1-123.86 71.5z m-312.54-768a84 84 0 0 0-72.54 41.88L138.34 487.48a84 84 0 0 0 0 83.76L294.6 842a84 84 0 0 0 72.54 41.88h312.54A84 84 0 0 0 752.22 842l156.26-270.66a84 84 0 0 0 0-83.76l-156.26-270.76a84 84 0 0 0-72.54-41.88z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M539.36 703.56a174.2 174.2 0 1 1 174.18-174.2 174.38 174.38 0 0 1-174.18 174.2z m0-289.12a114.94 114.94 0 1 0 114.92 114.92 115.06 115.06 0 0 0-114.92-114.92z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </Svg>
  );
};

IconShezhi.defaultProps = {
  size: 18,
};

export default IconShezhi;
