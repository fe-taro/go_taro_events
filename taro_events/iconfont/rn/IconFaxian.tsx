/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconFaxian: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M511 958A448 448 0 0 1 194.16 193.18a448 448 0 0 1 633.68 633.66A445.14 445.14 0 0 1 511 958z m0-844c-218.4 0-396 177.68-396 396s177.68 396 396 396 396-177.68 396-396-177.6-396-396-396z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M378.44 690.16A48.7 48.7 0 0 1 332 627.18l62.7-204.5a48.48 48.48 0 0 1 33.04-32.5L630 331.8a48.66 48.66 0 0 1 60.68 58.76l-48.18 189.56a48.56 48.56 0 0 1-31.58 34.1l-216.8 73.34a48.92 48.92 0 0 1-15.68 2.6z m65.44-250.52l-60.28 196.58 208.9-70.68 46.32-182.18z m200.52-57.88z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </Svg>
  );
};

IconFaxian.defaultProps = {
  size: 18,
};

export default IconFaxian;
