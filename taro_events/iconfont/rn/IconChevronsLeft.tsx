/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconChevronsLeft: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M534.186667 896c-13.610667 0-25.472-5.12-35.669334-15.274667l-312.576-310.869333c-20.352-20.394667-20.352-52.650667 0-71.338667l310.869334-312.576c20.394667-20.352 52.650667-20.352 71.338666 0 20.394667 20.394667 20.394667 52.693333 0 71.338667l-275.2 276.906667 275.2 275.2c20.394667 20.352 20.394667 52.650667 0 71.338666-8.490667 10.197333-22.058667 15.274667-33.962666 15.274667z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <Path
        d="M845.056 896c-13.610667 0-25.514667-5.12-35.712-15.274667l-312.533333-310.869333c-20.394667-20.394667-20.394667-52.650667 0-71.338667l312.533333-312.576c20.394667-20.352 52.693333-20.352 71.381333 0 20.352 20.394667 20.352 52.693333 0 71.338667l-275.2 276.906667 275.2 275.2c20.352 20.352 20.352 52.650667 0 71.338666a51.797333 51.797333 0 0 1-35.669333 15.274667z"
        fill={getIconColor(color, 1, '#333333')}
      />
    </Svg>
  );
};

IconChevronsLeft.defaultProps = {
  size: 18,
};

export default IconChevronsLeft;
