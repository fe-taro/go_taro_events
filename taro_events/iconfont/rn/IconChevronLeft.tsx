/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconChevronLeft: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M672.853333 896c-13.952 0-26.197333-5.12-36.693333-15.274667l-321.749333-310.869333c-20.992-20.394667-20.992-52.650667 0-71.338667l320-312.576c20.992-20.352 54.186667-20.352 73.429333 0 20.992 20.394667 20.992 52.693333 0 71.338667l-283.306667 276.906667 283.306667 275.2c20.992 20.352 20.992 52.650667 0 71.338666a47.232 47.232 0 0 1-34.986667 15.274667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </Svg>
  );
};

IconChevronLeft.defaultProps = {
  size: 18,
};

export default IconChevronLeft;
