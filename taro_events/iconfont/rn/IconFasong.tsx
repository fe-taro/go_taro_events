/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconFasong: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M226.28 749.02a188.14 188.14 0 1 1 188.12-188.12 188.34 188.34 0 0 1-188.12 188.12z m0-324.26a136.14 136.14 0 1 0 136.12 136.14 136.28 136.28 0 0 0-136.12-136.14zM765.52 888.2a145.56 145.56 0 1 1 145.56-145.56 145.72 145.72 0 0 1-145.56 145.56z m0-239.12a93.56 93.56 0 1 0 93.56 93.56 93.66 93.66 0 0 0-93.56-93.56z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M355.46 488.9a26 26 0 0 1-14.52-47.6l172.6-115.78a26 26 0 1 1 28.96 43.18L370 484.48a26 26 0 0 1-14.54 4.42z"
        fill={getIconColor(color, 1, '#231815')}
      />
      <Path
        d="M626.44 424.76A145.58 145.58 0 1 1 772 279.18a145.74 145.74 0 0 1-145.56 145.58z m0-239.16A93.58 93.58 0 1 0 720 279.18a93.68 93.68 0 0 0-93.56-93.58z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
      <Path
        d="M646 768.64a25.66 25.66 0 0 1-7.2-1.02l-290.58-83.74a26 26 0 1 1 14.4-50l290.54 83.74a26 26 0 0 1-7.2 50.98z"
        fill={getIconColor(color, 3, '#231815')}
      />
    </Svg>
  );
};

IconFasong.defaultProps = {
  size: 18,
};

export default IconFasong;
