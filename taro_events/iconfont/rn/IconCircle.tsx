/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconCircle: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M512 981.333333C253.013333 981.333333 42.666667 770.986667 42.666667 512S253.013333 42.666667 512 42.666667s469.333333 210.346667 469.333333 469.333333-210.346667 469.333333-469.333333 469.333333z m0-844.501333A375.125333 375.125333 0 0 0 136.832 512 375.125333 375.125333 0 0 0 512 887.168 375.125333 375.125333 0 0 0 887.168 512 375.125333 375.125333 0 0 0 512 136.832z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </Svg>
  );
};

IconCircle.defaultProps = {
  size: 18,
};

export default IconCircle;
