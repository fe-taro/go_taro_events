/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconChevronDown: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M491.52 725.333333c-13.610667 0-27.178667-5.248-35.669333-15.744l-312.576-321.706666c-20.352-21.034667-20.352-54.229333 0-73.472 20.394667-20.992 52.693333-20.992 71.338666 0l275.2 283.306666 276.906667-283.306666c20.352-20.992 52.650667-20.992 71.338667 0 20.352 20.992 20.352 54.186667 0 73.429333l-310.869334 321.749333c-10.197333 8.746667-23.765333 15.744-35.669333 15.744z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </Svg>
  );
};

IconChevronDown.defaultProps = {
  size: 18,
};

export default IconChevronDown;
