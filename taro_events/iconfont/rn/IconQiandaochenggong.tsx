/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconQiandaochenggong: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M819.7 882.24H225.4A139.54 139.54 0 0 1 86 742.86V320.48a139.54 139.54 0 0 1 139.38-139.4h594.32a139.56 139.56 0 0 1 139.4 139.4v422.38a139.54 139.54 0 0 1-139.4 139.38zM225.4 233.08a87.48 87.48 0 0 0-87.4 87.4v422.38a87.48 87.48 0 0 0 87.38 87.38h594.32a87.48 87.48 0 0 0 87.4-87.38V320.48a87.48 87.48 0 0 0-87.4-87.4z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M933.1 474H112a26 26 0 0 1 0-52h821.1a26 26 0 0 1 0 52z"
        fill={getIconColor(color, 1, '#231815')}
      />
      <Path
        d="M313.12 118.18m39.68 0l0.02 0q39.68 0 39.68 39.68l0 135.16q0 39.68-39.68 39.68l-0.02 0q-39.68 0-39.68-39.68l0-135.16q0-39.68 39.68-39.68Z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
      <Path
        d="M636.1 118.18m39.68 0l0.02 0q39.68 0 39.68 39.68l0 135.16q0 39.68-39.68 39.68l-0.02 0q-39.68 0-39.68-39.68l0-135.16q0-39.68 39.68-39.68Z"
        fill={getIconColor(color, 3, '#F6AE65')}
      />
      <Path
        d="M496 780a82.8 82.8 0 0 1-50.82-17.34l-89.6-69.7a26 26 0 0 1 32-41.04l89.6 69.7a30.88 30.88 0 0 0 32.52 3.24l197.34-205.46a26 26 0 0 1 37.5 36L542.18 766l-2.8 1.74A82.56 82.56 0 0 1 496 780z"
        fill={getIconColor(color, 4, '#F6AE65')}
      />
    </Svg>
  );
};

IconQiandaochenggong.defaultProps = {
  size: 18,
};

export default IconQiandaochenggong;
