/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconShuqian: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M323.1 430.26a48.72 48.72 0 0 1-48.7-48.68V119.04a26 26 0 0 1 52 0v254.82l53.28-50.4a48.74 48.74 0 0 1 68.8 1.88l1.06 1.2L486 370.1V119.04a26 26 0 1 1 52 0v260.28a48.68 48.68 0 0 1-84 33.46l-1.06-1.18-40.1-48-56.28 53.24a48.58 48.58 0 0 1-33.46 13.42z"
        fill={getIconColor(color, 0, '#F6AE65')}
      />
      <Path
        d="M788.64 962H265.28a140.84 140.84 0 0 1-140.68-140.74V232.44a140.84 140.84 0 0 1 140.68-140.7h523.36a140.86 140.86 0 0 1 140.7 140.7v588.82A140.86 140.86 0 0 1 788.64 962zM265.28 146.36a86.16 86.16 0 0 0-86 86v588.9a86.16 86.16 0 0 0 86 86h523.36a86.18 86.18 0 0 0 86-86V232.44a86.18 86.18 0 0 0-86-86z"
        fill={getIconColor(color, 1, '#231815')}
      />
    </Svg>
  );
};

IconShuqian.defaultProps = {
  size: 18,
};

export default IconShuqian;
