/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconYouhuiquan: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M387.46 751.44h-52v-80h52z m0-120h-52v-80h52z m0-120h-52v-80h52z m0-120h-52v-80h52z"
        fill={getIconColor(color, 0, '#F6AE65')}
      />
      <Path
        d="M839.38 867.7H474.64a84.58 84.58 0 0 1-63.62-28.88l-25.64-29.34a28.9 28.9 0 0 0-44.4 1.08l-19.84 25.08a84 84 0 0 1-66.26 32h-48.16a141.36 141.36 0 0 1-141.18-141.2V348.34a141.36 141.36 0 0 1 141.18-141.2h44.18a84.4 84.4 0 0 1 64.64 30l24.64 29.28a28.9 28.9 0 0 0 43.84 0.42l27-30.84a84.58 84.58 0 0 1 63.62-28.88h364.74a141.36 141.36 0 0 1 141.2 141.2v378.18a141.36 141.36 0 0 1-141.2 141.2zM363.66 744a84.24 84.24 0 0 1 63.58 28.88l25.66 29.34a28.84 28.84 0 0 0 21.74 9.78h364.74a85.68 85.68 0 0 0 85.58-85.58V348.34a85.68 85.68 0 0 0-85.58-85.56H474.64a28.82 28.82 0 0 0-21.74 9.86L426 303.52a84.5 84.5 0 0 1-128.26-1.2l-24.74-29.28a28.86 28.86 0 0 0-22-10.26h-44.28a85.66 85.66 0 0 0-85.56 85.56v378.16A85.68 85.68 0 0 0 206.72 812h48.16a28.7 28.7 0 0 0 22.64-10.96l19.86-25.04a84.28 84.28 0 0 1 64.18-32z"
        fill={getIconColor(color, 1, '#231815')}
      />
    </Svg>
  );
};

IconYouhuiquan.defaultProps = {
  size: 18,
};

export default IconYouhuiquan;
