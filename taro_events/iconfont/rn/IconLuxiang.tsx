/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconLuxiang: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M744.82 884.2H279.18a139.54 139.54 0 0 1-139.38-139.38V279.18a139.54 139.54 0 0 1 139.38-139.38h465.64a139.54 139.54 0 0 1 139.38 139.38v465.64a139.54 139.54 0 0 1-139.38 139.38zM279.18 191.8a87.48 87.48 0 0 0-87.38 87.38v465.64a87.48 87.48 0 0 0 87.38 87.38h465.64a87.48 87.48 0 0 0 87.38-87.38V279.18a87.48 87.48 0 0 0-87.38-87.38z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M457.62 676.72a83.04 83.04 0 0 1-82.7-82.84V436a82.72 82.72 0 0 1 124-71.62l136.82 79a82.68 82.68 0 0 1 0 143.24l-136.82 78.98a82.42 82.42 0 0 1-41.3 11.12z m0.16-271.62a31.58 31.58 0 0 0-15.5 4.22 30.16 30.16 0 0 0-15.36 26.68v158a30.7 30.7 0 0 0 46 26.58l136.82-78.98a30.68 30.68 0 0 0 0-53.16l-136.82-79a30 30 0 0 0-15.14-4.34z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </Svg>
  );
};

IconLuxiang.defaultProps = {
  size: 18,
};

export default IconLuxiang;
