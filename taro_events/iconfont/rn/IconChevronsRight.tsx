/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconChevronsRight: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M532.48 896c-13.610667 0-25.472-5.12-35.669333-15.274667-20.394667-20.394667-20.394667-52.693333 0-71.338666l275.2-275.2-275.2-276.906667c-20.394667-20.352-20.394667-52.650667 0-71.338667 20.394667-20.352 52.650667-20.352 71.338666 0l312.576 312.576c20.352 20.394667 20.352 52.650667 0 71.338667l-312.576 310.869333c-8.490667 10.197333-22.101333 15.274667-35.669333 15.274667z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <Path
        d="M221.610667 896c-13.568 0-25.472-5.12-35.669334-15.274667-20.352-20.394667-20.352-52.693333 0-71.338666l275.2-275.2-275.2-276.906667c-20.352-20.352-20.352-52.650667 0-71.338667a49.28 49.28 0 0 1 71.338667 0l312.576 312.576c20.394667 20.394667 20.394667 52.650667 0 71.338667L257.28 880.725333a48.896 48.896 0 0 1-35.669333 15.274667z"
        fill={getIconColor(color, 1, '#333333')}
      />
    </Svg>
  );
};

IconChevronsRight.defaultProps = {
  size: 18,
};

export default IconChevronsRight;
