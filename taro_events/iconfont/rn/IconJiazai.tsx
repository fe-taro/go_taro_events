/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconJiazai: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M505.36 883.46c-214.4 0-388.82-174.42-388.82-388.82S290.96 105.82 505.36 105.82A388.32 388.32 0 0 1 763.6 204a26 26 0 1 1-34.56 38.86 336.38 336.38 0 0 0-223.68-85c-185.72 0-336.82 151.1-336.82 336.82s151.1 336.82 336.82 336.82a336.76 336.76 0 0 0 279.32-148.52A26 26 0 0 1 827.76 712a388.68 388.68 0 0 1-322.4 171.46z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M760.88 275h-164a26 26 0 0 1 0-52h145.84l-31.54-146.56A26 26 0 0 1 762 65.5l35.36 164.3a37.34 37.34 0 0 1-36.5 45.2z m-14.34-34.26z"
        fill={getIconColor(color, 1, '#F6AE65')}
      />
    </Svg>
  );
};

IconJiazai.defaultProps = {
  size: 18,
};

export default IconJiazai;
