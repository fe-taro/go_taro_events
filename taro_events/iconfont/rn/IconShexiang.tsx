/* tslint:disable */
/* eslint-disable */

import React, { FunctionComponent } from 'react';
import { ViewProps } from 'react-native';
import { Svg, GProps, Path } from 'react-native-svg';
import { getIconColor } from './helper';

interface Props extends GProps, ViewProps {
  size?: number;
  color?: string | string[];
}

const IconShexiang: FunctionComponent<Props> = ({ size, color, ...rest }) => {
  return (
    <Svg viewBox="0 0 1024 1024" width={size} height={size} {...rest}>
      <Path
        d="M615.58 862.8H212.36a146 146 0 0 1-145.86-145.84v-410a146 146 0 0 1 145.86-145.76h403.22a146 146 0 0 1 145.86 145.84v410a146 146 0 0 1-145.86 145.76zM212.36 226.1a81.02 81.02 0 0 0-80.94 80.94v410A81.02 81.02 0 0 0 212.36 798h403.22a81.02 81.02 0 0 0 80.94-80.94v-410a81.02 81.02 0 0 0-80.94-80.94z"
        fill={getIconColor(color, 0, '#231815')}
      />
      <Path
        d="M889.18 710.46a87.66 87.66 0 0 1-24.5-3.54l-1.46-0.42-159.74-66.98V384.48l159.74-66.98 1.46-0.42a85.62 85.62 0 0 1 76 13.78 83.46 83.46 0 0 1 33.82 67.3v227.68a83.54 83.54 0 0 1-33.82 67.32 85.62 85.62 0 0 1-51.5 17.3zM880.9 656a32.68 32.68 0 0 0 27.72-5.54 30.48 30.48 0 0 0 12.42-24.52V398.16a30.42 30.42 0 0 0-12.42-24.5 32.54 32.54 0 0 0-27.72-5.56l-124 52V604z"
        fill={getIconColor(color, 1, '#231815')}
      />
      <Path
        d="M233.9 311.44m24 0l128.64 0q24 0 24 24l0 9.8q0 24-24 24l-128.64 0q-24 0-24-24l0-9.8q0-24 24-24Z"
        fill={getIconColor(color, 2, '#F6AE65')}
      />
    </Svg>
  );
};

IconShexiang.defaultProps = {
  size: 18,
};

export default IconShexiang;
