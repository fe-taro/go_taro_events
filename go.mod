module go_taro_event

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.2
	github.com/pkg/errors v0.8.1
	github.com/spf13/viper v1.7.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gorm.io/driver/mysql v1.1.0
	gorm.io/gorm v1.21.10
)
