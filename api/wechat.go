package api

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"go_taro_event/model"
	"go_taro_event/response"
	"go_taro_event/utils"
	"net/http"
)

//type WXLoginResp  model.WXLoginResp

// 微信小程序登录
func AppletWeChatLogin(ctx *gin.Context) {
	code := ctx.Query("code")     //  获取code
	// 根据code获取 openID 和 session_key
	wxLoginResp,err := WXLogin(code)
	if err != nil {
		response.ResCommon(ctx, http.StatusUnprocessableEntity, 400, nil, err.Error())
		return
	}
	// 保存登录态
	//session := sessions.Default(ctx)
	//session.Set("openid", wxLoginResp.OpenId)
	//session.Set("sessionKey", wxLoginResp.SessionKey )

	// 这里用openid和sessionkey的串接 进行MD5之后作为该用户的自定义登录态
	mySession := utils.GetMD5Encode(wxLoginResp.OpenId + wxLoginResp.SessionKey)
	// 接下来可以将openid 和 sessionkey, mySession 存储到数据库中,
	// 但这里要保证mySession 唯一, 以便于用mySession去索引openid 和sessionkey
	response.Success(ctx, gin.H{
		"session": mySession,
	}, "登录成功")

}


// 这个函数以 code 作为输入, 返回调用微信接口得到的对象指针和异常情况
func WXLogin(code string) (* model.WXLoginResp, error) {
	url := "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code"
	// 合成url, 这里的appId和secret是在微信公众平台上获取的
	//url = fmt.Sprintf(url, appId, secret, code)
	url = fmt.Sprintf(url, "wx6cb5b2cdb40fd0bc", "264b30570f96697f098fb84878096cb0", code)

	// 创建http get请求
	resp,err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// 解析http请求中body 数据到我们定义的结构体中
	wxResp := model.WXLoginResp{}
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&wxResp); err != nil {
		return nil, err
	}

	// 判断微信接口返回的是否是一个异常情况
	if wxResp.ErrCode != 0 {
		return nil, errors.New(fmt.Sprintf("ErrCode:%s  ErrMsg:%s", wxResp.ErrCode, wxResp.ErrMsg))
	}

	return &wxResp, nil
}

