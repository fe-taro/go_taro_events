package route

import (
	"github.com/gin-gonic/gin"
	"go_taro_event/api"
	"go_taro_event/middleware"
)

func RouterCollect(r * gin.Engine) * gin.Engine  {
	r.Use(middleware.CORSMiddleware())
	r.POST("/api/auth/register", api.Register)
	r.POST("/api/auth/login", api.Login)
	r.GET("/api/auth/userinfo", middleware.AuthMiddleware(), api.Userinfo)
	r.GET("/api/wx/Appletlogin", api.AppletWeChatLogin)
	return r
}